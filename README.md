# cash-service
**REST API**  
Postman collection available [here](./doc/rest/Cash Service test task.postman_collection.json)   
Swagger UI available at http://localhost:8080/swagger-ui.html

**Задание (RUS)**  
Должно быть 3 API (RESTful):
- перевод денег с одного счёта на другой,
- положить деньги на счёт,
- снять деньги со счёта.

Отрицательный баланс счета недопустим.
В качестве хранилища можно использовать любую in-memory БД.
Доступ к БД осуществить через JPA.

Исходный код должен собираться с помощью maven или gradle в исполняемый jar.
Решение должно быть на Java или Kotlin

**Task**  
Next 3 API endpoints required (RESTful):  
- transfer cash from one cash account to destination  
- add cash to cash account  
- withdraw cash from cash account  

Negative balance is prohibited.  
It is recommended to use any in-memory DB as storage.  
Implement DB access using JPA.  

It should be possible to compile source code using maven or gradle into jar file.  
Source code must be in Java or Kotlin.  