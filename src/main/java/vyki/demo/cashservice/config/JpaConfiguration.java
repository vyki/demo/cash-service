package vyki.demo.cashservice.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = {"vyki.demo.cashservice.repository"})
@EntityScan(basePackages = {"vyki.demo.cashservice.model.entity"})
@Configuration
public class JpaConfiguration {
}
