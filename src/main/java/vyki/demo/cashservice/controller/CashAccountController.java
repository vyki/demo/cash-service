package vyki.demo.cashservice.controller;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vyki.demo.cashservice.model.dto.operations.AddCashDto;
import vyki.demo.cashservice.model.dto.operations.TransferCashDto;
import vyki.demo.cashservice.model.dto.operations.WithdrawCashDto;
import vyki.demo.cashservice.model.entity.CashAccount;
import vyki.demo.cashservice.service.CashAccountService;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping(CashAccountController.CONTROLLER_BASEPATH)
@RequiredArgsConstructor
public class CashAccountController {

    public static final String CONTROLLER_BASEPATH = "/v1/cashaccount";
    private final CashAccountService cashAccountService;

    // for test purposes, not required in the task
    @GetMapping
    public Collection<CashAccount> getAllCashAccounts() {
        return cashAccountService.getAllCashAccounts();
    }

    @PostMapping("addCash")
    @ApiOperation("Add cash to cash account")
    @SneakyThrows
    public ResponseEntity<String> addCash(@RequestBody @Valid AddCashDto addCashDto) {
        cashAccountService.addCash(addCashDto);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("withdrawCash")
    @ApiOperation("Withdraw cash from cash account")
    @SneakyThrows
    public ResponseEntity<String> withdrawCash(@RequestBody @Valid WithdrawCashDto withdrawCashDto) {
        cashAccountService.withdrawCash(withdrawCashDto);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("transferCash")
    @ApiOperation("Transfer cash from one cash account to destination")
    @SneakyThrows
    public ResponseEntity<String> transferCash(@RequestBody @Valid TransferCashDto transferCashDto) {
        cashAccountService.transferCash(transferCashDto);
        return ResponseEntity.noContent().build();
    }
}
