package vyki.demo.cashservice.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import vyki.demo.cashservice.model.dto.operations.AddCashDto;
import vyki.demo.cashservice.model.dto.operations.TransferCashDto;

@Component
public class TransferCashDtoToAddCashDtoConverter implements Converter<TransferCashDto, AddCashDto> {
    @Override
    public AddCashDto convert(TransferCashDto transferCashDto) {
        return new AddCashDto(transferCashDto.getDestinationCashAccountId(), transferCashDto.getCashValueToTransfer());
    }
}
