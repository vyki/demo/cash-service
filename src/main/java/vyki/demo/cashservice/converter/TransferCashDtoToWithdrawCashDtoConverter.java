package vyki.demo.cashservice.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import vyki.demo.cashservice.model.dto.operations.TransferCashDto;
import vyki.demo.cashservice.model.dto.operations.WithdrawCashDto;

@Component
public class TransferCashDtoToWithdrawCashDtoConverter implements Converter<TransferCashDto, WithdrawCashDto> {
    @Override
    public WithdrawCashDto convert(TransferCashDto transferCashDto) {
        return new WithdrawCashDto(transferCashDto.getCashAccountId(), transferCashDto.getCashValueToTransfer());
    }
}
