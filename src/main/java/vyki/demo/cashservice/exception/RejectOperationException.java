package vyki.demo.cashservice.exception;

public class RejectOperationException extends RuntimeException {
    public RejectOperationException() {
        super();
    }
    public RejectOperationException(String s) {
        super(s);
    }
    public RejectOperationException(String s, Throwable throwable) {
        super(s, throwable);
    }
    public RejectOperationException(Throwable throwable) {
        super(throwable);
    }
}
