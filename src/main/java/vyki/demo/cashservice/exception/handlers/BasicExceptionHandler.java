package vyki.demo.cashservice.exception.handlers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import vyki.demo.cashservice.exception.NotFoundException;
import vyki.demo.cashservice.exception.RejectOperationException;
import vyki.demo.cashservice.model.dto.response.BadResponseDto;

import static org.apache.commons.lang3.ArrayUtils.toArray;

@Slf4j
@ControllerAdvice(basePackages="vyki.demo.cashservice.controller")
public class BasicExceptionHandler {

    @ExceptionHandler(BindException.class)
    public ResponseEntity<BadResponseDto> handleAnnotationValidations(BindException ex) {
        return buildBadResponse(ex.getClass(), HttpStatus.BAD_REQUEST, ex.getBindingResult()
                .getAllErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .toArray(String[]::new));
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<BadResponseDto> handleNotFound(NotFoundException ex) {
        log.error(ex.getMessage(), ex);
        return buildBadResponse(ex.getClass(), HttpStatus.NOT_FOUND, toArray(ex.getMessage()));
    }

    @ExceptionHandler(RejectOperationException.class)
    public ResponseEntity<BadResponseDto> handleRejectOperation(RejectOperationException ex) {
        log.error(ex.getMessage(), ex);
        return buildBadResponse(ex.getClass(), HttpStatus.CONFLICT, toArray(ex.getMessage()));
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<BadResponseDto> handleBodyNotReadable(HttpMessageNotReadableException ex) {
        log.error(ex.getMessage(), ex);
        return buildBadResponse(ex.getClass(), HttpStatus.BAD_REQUEST, toArray(ex.getMessage()));
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<BadResponseDto> handleOther(RuntimeException ex) {
        log.error(ex.getMessage(), ex);
        return buildBadResponse(ex.getClass(), HttpStatus.INTERNAL_SERVER_ERROR, toArray(ex.getMessage()));
    }

    private ResponseEntity<BadResponseDto> buildBadResponse(Class<? extends Exception> exceptionClass,
                                                            HttpStatus httpStatus,
                                                            String[] messages) {
        BadResponseDto responseBody = BadResponseDto.builder()
                .timestamp(System.currentTimeMillis())
                .status(httpStatus.value())
                .exception(exceptionClass.getSimpleName())
                .messages(messages)
                .build();
        return new ResponseEntity<>(responseBody, httpStatus);
    }
}
