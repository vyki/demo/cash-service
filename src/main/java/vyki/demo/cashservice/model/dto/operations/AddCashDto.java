package vyki.demo.cashservice.model.dto.operations;

import lombok.AllArgsConstructor;
import lombok.Data;
import vyki.demo.cashservice.validation.annotations.CorrectUuid;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@Data
public class AddCashDto implements CashOperation {

    @NotNull(message = "Field cashAccountId must contain not null UUID")
    @CorrectUuid(message = "cashAccountId must be in UUID format")
    private String cashAccountId;

    @NotNull(message = "Field cashValueToAdd must contain not null number")
    @Min(value = 1, message = "cashValueToAdd minimal value is 1")
    private Double cashValueToAdd;
}
