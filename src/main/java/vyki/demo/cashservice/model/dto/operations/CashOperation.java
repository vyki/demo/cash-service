package vyki.demo.cashservice.model.dto.operations;

public interface CashOperation {
    String getCashAccountId();
}
