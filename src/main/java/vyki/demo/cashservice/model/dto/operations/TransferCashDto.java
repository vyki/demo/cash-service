package vyki.demo.cashservice.model.dto.operations;

import lombok.AllArgsConstructor;
import lombok.Data;
import vyki.demo.cashservice.validation.annotations.CorrectUuid;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@Data
public class TransferCashDto implements CashOperation {

    @NotNull(message = "Field cashAccountId must contain not null UUID")
    @CorrectUuid(message = "cashAccountId must be in UUID format")
    private String cashAccountId;

    @NotNull(message = "Field destinationCashAccountId must contain not null UUID")
    @CorrectUuid(message = "destinationCashAccountId must be in UUID format")
    private String destinationCashAccountId;

    @NotNull(message = "Field cashValueToTransfer must contain not null number")
    @Min(value = 1, message = "cashValueToTransfer minimal value is 1")
    private Double cashValueToTransfer;
}
