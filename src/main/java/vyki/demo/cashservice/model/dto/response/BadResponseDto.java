package vyki.demo.cashservice.model.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BadResponseDto {
    private long timestamp;
    private int status;
    private String[] messages;
    private String exception;
}
