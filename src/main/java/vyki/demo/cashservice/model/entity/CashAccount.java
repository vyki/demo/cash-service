package vyki.demo.cashservice.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "CASH_ACCOUNT")
public class CashAccount {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    private String ownerName;

    @Column(nullable = false, columnDefinition = "Decimal(10,2) default 0")
    private Double cashValue;

    public void addValue(Double value) {
        cashValue += value;
    }

    public void subtractValue(Double value) {
        cashValue -= value;
    }
}
