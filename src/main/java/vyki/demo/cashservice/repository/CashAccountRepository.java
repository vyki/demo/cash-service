package vyki.demo.cashservice.repository;

import org.springframework.data.repository.CrudRepository;
import vyki.demo.cashservice.model.entity.CashAccount;

import java.util.UUID;

public interface CashAccountRepository extends CrudRepository<CashAccount, UUID> {
}

