package vyki.demo.cashservice.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import vyki.demo.cashservice.exception.NotFoundException;
import vyki.demo.cashservice.exception.RejectOperationException;
import vyki.demo.cashservice.model.dto.operations.AddCashDto;
import vyki.demo.cashservice.model.dto.operations.CashOperation;
import vyki.demo.cashservice.model.dto.operations.TransferCashDto;
import vyki.demo.cashservice.model.dto.operations.WithdrawCashDto;
import vyki.demo.cashservice.model.entity.CashAccount;
import vyki.demo.cashservice.repository.CashAccountRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;

@Service
@RequiredArgsConstructor
public class CashAccountService {

    public static final String CASH_ACCOUNT_NOT_FOUND_BY_ID = "CashAccount not found by id: ";
    public static final String CASH_WITHDRAW_BALANCE_REJECT = "Unable to perform cash withdrawal operation for CashAccount: %s. " +
            "Current balance %s less then withdraw amount %s";

    private final CashAccountRepository cashAccountRepository;
    private final ConversionService conversionService;
    private final KeyLockExecutorService keyLockExecutorService;

    // for test purposes, not required in the task
    public Collection<CashAccount> getAllCashAccounts() {
        List<CashAccount> cashAccounts = new ArrayList<>();
        cashAccountRepository.findAll().forEach(cashAccounts::add);
        return cashAccounts;
    }

    @SneakyThrows
    public void addCash(AddCashDto addCashDto) {
        keyLockExecutorService.executeWithKeyLock(addCashDto.getCashAccountId(),
                addCashCallable(addCashDto));
    }

    @SneakyThrows
    public void withdrawCash(WithdrawCashDto withdrawCashDto) {
        keyLockExecutorService.executeWithKeyLock(withdrawCashDto.getCashAccountId(),
                withdrawCashCallable(withdrawCashDto));
    }

    @SneakyThrows
    public void transferCash(TransferCashDto transferCashDto) {
        Callable<?> transferCallable = () -> {
            WithdrawCashDto withdrawCashDto = conversionService.convert(transferCashDto, WithdrawCashDto.class);
            AddCashDto addCashDto = conversionService.convert(transferCashDto, AddCashDto.class);
            withdrawCashCallable(withdrawCashDto).call();
            addCashCallable(addCashDto).call();
            return null;
        };
        keyLockExecutorService.executeWithKeyLock(transferCashDto.getCashAccountId(),
                transferCashDto.getDestinationCashAccountId(),
                transferCallable);
    }

    private CashAccount getCashAccountById(CashOperation cashOperation) {
        UUID cashAccountId = UUID.fromString(cashOperation.getCashAccountId());
        return cashAccountRepository.findById(cashAccountId)
                .orElseThrow(() -> new NotFoundException(CASH_ACCOUNT_NOT_FOUND_BY_ID + cashAccountId));
    }

    private Callable<?> addCashCallable(AddCashDto addCashDto) {
        return () -> {
            CashAccount cashAccount = getCashAccountById(addCashDto);
            cashAccount.addValue(addCashDto.getCashValueToAdd());
            cashAccountRepository.save(cashAccount);
            return null;
        };
    }

    private Callable<?> withdrawCashCallable(WithdrawCashDto withdrawCashDto) {
        return () -> {
            CashAccount cashAccount = getCashAccountById(withdrawCashDto);
            Double currentCashValue = cashAccount.getCashValue();
            Double cashValueToWithdraw = withdrawCashDto.getCashValueToWithdraw();
            if (currentCashValue < cashValueToWithdraw) throw new RejectOperationException(
                    String.format(CASH_WITHDRAW_BALANCE_REJECT, cashAccount.getId(), cashAccount.getCashValue(), withdrawCashDto.getCashValueToWithdraw()));
            cashAccount.subtractValue(cashValueToWithdraw);
            cashAccountRepository.save(cashAccount);
            return null;
        };
    }
}
