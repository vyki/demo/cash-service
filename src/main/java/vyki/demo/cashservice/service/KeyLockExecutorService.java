package vyki.demo.cashservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ConcurrentReferenceHashMap;

import java.util.concurrent.Callable;

@Service
@Slf4j
public class KeyLockExecutorService {
    private final ConcurrentReferenceHashMap<Object, Object> keyLockMap;

    public KeyLockExecutorService() {
        this.keyLockMap = new ConcurrentReferenceHashMap<>();
    }

    public <T> T executeWithKeyLock(Object key, Callable<T> callable) throws Exception {
        synchronized (getLockByKey(key)) {
            return callable.call();
        }
    }

    public <T> T executeWithKeyLock(Object key1, Object key2, Callable<T> callable) throws Exception {
        Object priorityKey = key1.hashCode() > key2.hashCode() ? key1 : key2;
        Object secondKey = key1.equals(priorityKey) ? key2 : key1;
        synchronized (getLockByKey(priorityKey)) {
            synchronized (getLockByKey(secondKey)) {
                return callable.call();
            }
        }
    }

    private Object getLockByKey(Object key) {
        return keyLockMap.compute(key, (k, v) -> v == null ? new Object() : v);
    }
}
