CREATE TABLE cash_account
(
    id UUID NOT NULL,
    owner_name character varying NOT NULL,
    cash_value decimal(10,2) default 0 NOT NULL,
    CONSTRAINT cash_account_pk PRIMARY KEY (id)
);
