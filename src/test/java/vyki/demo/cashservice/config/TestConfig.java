package vyki.demo.cashservice.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("test")
@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = {
        "vyki.demo.cashservice"
})
public class TestConfig {
}
