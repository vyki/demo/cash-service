package vyki.demo.cashservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import vyki.demo.cashservice.service.CashAccountService;

import static vyki.demo.cashservice.controller.CashAccountController.CONTROLLER_BASEPATH;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@WebMvcTest(CashAccountController.class)
class CashAccountControllerTest {

    private static final String ADD_CASH = "/addCash";
    private static final String WITHDRAW_CASH = "/withdrawCash";
    private static final String TRANSFER_CASH = "/transferCash";

    @MockBean
    private CashAccountService cashAccountService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    // addCash tests

    @Test
    void addCash_sunny_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post(CONTROLLER_BASEPATH + "/addCash")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"cashAccountId\": \"a1a64c26-4735-42a9-9894-8f30ef1dd971\",\n" +
                        "    \"cashValueToAdd\": 10\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    void addCash_wrongIdFormat_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post(CONTROLLER_BASEPATH + ADD_CASH)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"cashAccountId\": \"123\",\n" +
                        "    \"cashValueToAdd\": 10\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.messages[0]").value("cashAccountId must be in UUID format"));
    }

    @Test
    void addCash_wrongAddValue_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post(CONTROLLER_BASEPATH + ADD_CASH)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"cashAccountId\": \"a1a64c26-4735-42a9-9894-8f30ef1dd971\",\n" +
                        "    \"cashValueToAdd\": -1\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.messages[0]").value("cashValueToAdd minimal value is 1"));
    }

    @Test
    void addCash_bodyNotReadable_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post(CONTROLLER_BASEPATH + ADD_CASH)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"cashAccountId\": \"a1a64c26-4735-42a9-9894-8f30ef1dd971\",\n" +
                        "    \"cashValueToAdd\": \"zz\"\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.exception").value("HttpMessageNotReadableException"));
    }

    @Test
    void addCash_nullFields_2messages_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post(CONTROLLER_BASEPATH + ADD_CASH)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"cashAccountId\": null,\n" +
                        "    \"cashValueToAdd\": null\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.messages[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.messages[1]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.messages[2]").doesNotExist());
    }

    // withdraw tests
    @Test
    void withdrawCash_sunny_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post(CONTROLLER_BASEPATH + WITHDRAW_CASH)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"cashAccountId\": \"a1a64c26-4735-42a9-9894-8f30ef1dd971\",\n" +
                        "    \"cashValueToWithdraw\": 10\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    void withdrawCash_wrongIdFormat_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post(CONTROLLER_BASEPATH + WITHDRAW_CASH)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"cashAccountId\": \"123\",\n" +
                        "    \"cashValueToWithdraw\": 10\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.messages[0]").value("cashAccountId must be in UUID format"));
    }

    @Test
    void withdrawCash_wrongAddValue_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post(CONTROLLER_BASEPATH + WITHDRAW_CASH)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"cashAccountId\": \"a1a64c26-4735-42a9-9894-8f30ef1dd971\",\n" +
                        "    \"cashValueToWithdraw\": -1\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.messages[0]").value("cashValueToWithdraw minimal value is 1"));
    }

    @Test
    void withdrawCash_bodyNotReadable_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post(CONTROLLER_BASEPATH + WITHDRAW_CASH)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"cashAccountId\": \"a1a64c26-4735-42a9-9894-8f30ef1dd971\",\n" +
                        "    \"cashValueToWithdraw\": \"zz\"\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.exception").exists());
    }

    @Test
    void withdrawCash_nullFields_2messages_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post(CONTROLLER_BASEPATH + WITHDRAW_CASH)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"cashAccountId\": null,\n" +
                        "    \"cashValueToWithdraw\": null\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.messages[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.messages[1]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.messages[2]").doesNotExist());
    }

    // transfer tests
    @Test
    void transferCash_sunny_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post(CONTROLLER_BASEPATH + TRANSFER_CASH)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"cashAccountId\": \"a1a64c26-4735-42a9-9894-8f30ef1dd971\",\n" +
                        "    \"destinationCashAccountId\": \"a1a64c26-4735-42a9-9894-8f30ef1dd972\",\n" +
                        "    \"cashValueToTransfer\": 10\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    void transferCash_wrongIdFormat_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post(CONTROLLER_BASEPATH + TRANSFER_CASH)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"cashAccountId\": \"123\",\n" +
                        "    \"destinationCashAccountId\": \"a1a64c26-4735-42a9-9894-8f30ef1dd972\",\n" +
                        "    \"cashValueToTransfer\": 10\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.messages[0]").value("cashAccountId must be in UUID format"));
    }

    @Test
    void transferCash_wrongDestinationIdFormat_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post(CONTROLLER_BASEPATH + TRANSFER_CASH)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"cashAccountId\": \"a1a64c26-4735-42a9-9894-8f30ef1dd971\",\n" +
                        "    \"destinationCashAccountId\": \"123\",\n" +
                        "    \"cashValueToTransfer\": 10\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.messages[0]").value("destinationCashAccountId must be in UUID format"));
    }

    @Test
    void transferCash_wrongAddValue_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post(CONTROLLER_BASEPATH + TRANSFER_CASH)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"cashAccountId\": \"a1a64c26-4735-42a9-9894-8f30ef1dd971\",\n" +
                        "    \"destinationCashAccountId\": \"a1a64c26-4735-42a9-9894-8f30ef1dd972\",\n" +
                        "    \"cashValueToTransfer\": -1\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.messages[0]").value("cashValueToTransfer minimal value is 1"));
    }

    @Test
    void transferCash_bodyNotReadable_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post(CONTROLLER_BASEPATH + TRANSFER_CASH)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"cashAccountId\": \"a1a64c26-4735-42a9-9894-8f30ef1dd971\",\n" +
                        "    \"destinationCashAccountId\": \"a1a64c26-4735-42a9-9894-8f30ef1dd972\",\n" +
                        "    \"cashValueToTransfer\": \"zz\"\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.exception").exists());
    }

    @Test
    void transferCash_nullFields_2messages_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post(CONTROLLER_BASEPATH + TRANSFER_CASH)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"cashAccountId\": null,\n" +
                        "    \"destinationCashAccountId\": null,\n" +
                        "    \"cashValueToTransfer\": null\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.messages[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.messages[1]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.messages[2]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.messages[3]").doesNotExist());
    }

}