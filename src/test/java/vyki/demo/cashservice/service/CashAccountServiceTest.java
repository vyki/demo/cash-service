package vyki.demo.cashservice.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import vyki.demo.cashservice.config.TestConfig;
import vyki.demo.cashservice.exception.RejectOperationException;
import vyki.demo.cashservice.model.dto.operations.AddCashDto;
import vyki.demo.cashservice.model.dto.operations.TransferCashDto;
import vyki.demo.cashservice.model.dto.operations.WithdrawCashDto;
import vyki.demo.cashservice.model.entity.CashAccount;
import vyki.demo.cashservice.repository.CashAccountRepository;

import java.util.Collection;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@SpringBootTest
@ContextConfiguration(classes = TestConfig.class)
class CashAccountServiceTest {

    private static final String ADD_ACCOUNT = UUID.randomUUID().toString();
    private static final String WITHDRAW_ACCOUNT = UUID.randomUUID().toString();
    private static final String WITHDRAW_ACCOUNT2 = UUID.randomUUID().toString();
    private static final String TRANSFER_FROM_ACCOUNT = UUID.randomUUID().toString();
    private static final String TRANSFER_TO_ACCOUNT = UUID.randomUUID().toString();

    @Autowired
    private CashAccountService cashAccountService;
    @Autowired
    private CashAccountRepository cashAccountRepository;

    @BeforeEach
    public void setUp() {
        cashAccountRepository.save(new CashAccount(UUID.fromString(ADD_ACCOUNT), "Kurt Cobain", 0d));
        cashAccountRepository.save(new CashAccount(UUID.fromString(WITHDRAW_ACCOUNT), "John Petrucci", 100d));
        cashAccountRepository.save(new CashAccount(UUID.fromString(WITHDRAW_ACCOUNT2), "David Gilmour", 100000d));
        cashAccountRepository.save(new CashAccount(UUID.fromString(TRANSFER_FROM_ACCOUNT), "John Lennon", 100000d));
        cashAccountRepository.save(new CashAccount(UUID.fromString(TRANSFER_TO_ACCOUNT), "Roger Waters", 0d));
    }

    @Test
    void getAllCashAccounts_test() {
        Collection<CashAccount> allCashAccounts = cashAccountService.getAllCashAccounts();
        assertEquals(5, allCashAccounts.size());
    }

    @Test
    void addCash_sunny_test() {
        cashAccountService.addCash(new AddCashDto(ADD_ACCOUNT, 10.5));
        assertEquals(10.5, getAccountValue(ADD_ACCOUNT));
    }

    @Test
    void withdrawCash_sunny_test() {
        cashAccountService.withdrawCash(new WithdrawCashDto(WITHDRAW_ACCOUNT, 10.5));
        assertEquals(89.5, getAccountValue(WITHDRAW_ACCOUNT));
    }

    @Test
    void withdrawCash_exceedingAmount_test() {
        assertThrows(
                RejectOperationException.class,
                () -> cashAccountService.withdrawCash(new WithdrawCashDto(WITHDRAW_ACCOUNT2, 200000d)),
                "Operation must be Rejected, because balance amount less than withdraw value");
    }

    @Test
    void transferCash_sunny_test() {
        cashAccountService.transferCash(new TransferCashDto(TRANSFER_FROM_ACCOUNT, TRANSFER_TO_ACCOUNT, 20000.7));
        assertEquals(79999.3, getAccountValue(TRANSFER_FROM_ACCOUNT));
        assertEquals(20000.7, getAccountValue(TRANSFER_TO_ACCOUNT));
    }

    @Test
    void transferCash_exceedingAmount_test() {
        assertThrows(
                RejectOperationException.class,
                () -> cashAccountService.transferCash(new TransferCashDto(TRANSFER_FROM_ACCOUNT, TRANSFER_TO_ACCOUNT, 200000d)),
                "Operation must be Rejected, because balance amount less than withdraw value");
    }

    private Double getAccountValue(String accountId) {
        return cashAccountRepository.findById(UUID.fromString(accountId))
                .map(CashAccount::getCashValue)
                .orElse(null);
    }
}