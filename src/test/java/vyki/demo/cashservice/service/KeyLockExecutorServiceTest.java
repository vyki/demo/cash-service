package vyki.demo.cashservice.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import vyki.demo.cashservice.config.TestConfig;
import vyki.demo.cashservice.model.dto.operations.AddCashDto;
import vyki.demo.cashservice.model.dto.operations.TransferCashDto;
import vyki.demo.cashservice.model.dto.operations.WithdrawCashDto;
import vyki.demo.cashservice.model.entity.CashAccount;
import vyki.demo.cashservice.repository.CashAccountRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@SpringBootTest
@ContextConfiguration(classes = TestConfig.class)
@Slf4j
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
class KeyLockExecutorServiceTest {

    private static final String SOURCE_ACCOUNT_1 = "a1a64c26-4735-42a9-9894-8f30ef1dd973";
    private static final String SOURCE_ACCOUNT_2 = "a1a64c26-4735-42a9-9894-8f30ef1dd974";
    private static final String DESTINATION_ACCOUNT_1 = "a1a64c26-4735-42a9-9894-8f30ef1dd971";
    private static final String DESTINATION_ACCOUNT_2 = "a1a64c26-4735-42a9-9894-8f30ef1dd972";

    @Autowired
    private CashAccountService cashAccountService;
    @Autowired
    private CashAccountRepository cashAccountRepository;

    private ExecutorService executorService;
    private List<Future<?>> futures;

    @BeforeEach
    void setUp() {
        executorService = Executors.newFixedThreadPool(20);
        futures = new LinkedList<>();
        cashAccountRepository.save(new CashAccount(UUID.fromString(DESTINATION_ACCOUNT_1), "Kurt Cobain", 0d));
        cashAccountRepository.save(new CashAccount(UUID.fromString(DESTINATION_ACCOUNT_2), "John Petrucci", 100d));
        cashAccountRepository.save(new CashAccount(UUID.fromString(SOURCE_ACCOUNT_1), "David Gilmour", 100000d));
        cashAccountRepository.save(new CashAccount(UUID.fromString(SOURCE_ACCOUNT_2), "John Lennon", 100000d));
    }

    @Test // indirect test via cashAccountService
    void executeWithKeyLock_addCash_1key_test() throws Exception {
        for (int i=0; i < 1000; i++) {
            AddCashDto addCashDto = new AddCashDto(DESTINATION_ACCOUNT_1, 1d);
            Future<?> future = executorService.submit(() -> cashAccountService.addCash(addCashDto));
            futures.add(future);
        }
        for (Future<?> future : futures) {
            future.get();
        }
        assertEquals(1000, getAccountValue(DESTINATION_ACCOUNT_1));
    }

    @Test // indirect test via cashAccountService
    void executeWithKeyLock_withdrawCash_1key_test() throws Exception {
        for (int i=0; i < 1000; i++) {
            WithdrawCashDto withdrawCashDto = new WithdrawCashDto(DESTINATION_ACCOUNT_2, 0.1d);
            Future<?> future = executorService.submit(() -> cashAccountService.withdrawCash(withdrawCashDto));
            futures.add(future);
        }
        for (Future<?> future : futures) {
            future.get();
        }
        assertEquals(0, getAccountValue(DESTINATION_ACCOUNT_1));
    }

    @Test // indirect test via cashAccountService
    void executeWithKeyLock_transferCash_2keys_test() throws Exception {
        for (int i=0; i < 5000; i++) {
            TransferCashDto transferCashDto1;
            TransferCashDto transferCashDto2;
            if (i % 2 == 0) {
                transferCashDto1 = new TransferCashDto(SOURCE_ACCOUNT_1, DESTINATION_ACCOUNT_1, 1d);
                transferCashDto2 = new TransferCashDto(SOURCE_ACCOUNT_1, DESTINATION_ACCOUNT_2, 1d);
            } else {
                transferCashDto1 = new TransferCashDto(SOURCE_ACCOUNT_2, DESTINATION_ACCOUNT_1, 1d);
                transferCashDto2 = new TransferCashDto(SOURCE_ACCOUNT_2, DESTINATION_ACCOUNT_2, 1d);
            }
            Future<?> future1 = executorService.submit(() -> cashAccountService.transferCash(transferCashDto1));
            Future<?> future2 = executorService.submit(() -> cashAccountService.transferCash(transferCashDto2));
            futures.add(future1);
            futures.add(future2);
        }
        for (Future<?> future : futures) {
            future.get();
        }
        assertEquals(95000, getAccountValue(SOURCE_ACCOUNT_1));
        assertEquals(95000, getAccountValue(SOURCE_ACCOUNT_2));
        assertEquals(5000, getAccountValue(DESTINATION_ACCOUNT_1));
        assertEquals(5100, getAccountValue(DESTINATION_ACCOUNT_2));
    }

    @Test // indirect test via cashAccountService
    void executeWithKeyLock_transferCash_2keys_reciprocally_test() throws Exception {
        for (int i=0; i < 5000; i++) {
            TransferCashDto transferCashDto;
            if (i % 2 == 0) {
                transferCashDto = new TransferCashDto(SOURCE_ACCOUNT_1, SOURCE_ACCOUNT_2, 1d);
            } else {
                transferCashDto = new TransferCashDto(SOURCE_ACCOUNT_2, SOURCE_ACCOUNT_1, 1d);
            }
            Future<?> future = executorService.submit(() -> cashAccountService.transferCash(transferCashDto));
            futures.add(future);
        }
        for (Future<?> future : futures) {
            future.get();
        }
        assertEquals(100000, getAccountValue(SOURCE_ACCOUNT_1));
        assertEquals(100000, getAccountValue(SOURCE_ACCOUNT_2));

    }

    private Double getAccountValue(String accountId) {
        return cashAccountRepository.findById(UUID.fromString(accountId))
                .map(CashAccount::getCashValue)
                .orElse(null);
    }

}